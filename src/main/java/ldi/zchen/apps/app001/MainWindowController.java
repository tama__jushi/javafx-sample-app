package ldi.zchen.apps.app001;

import java.awt.event.ActionEvent;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class MainWindowController implements Initializable {

	Logger logger = LoggerFactory.getLogger(MainWindowController.class);

	@FXML
	TextArea textAreaLog;
	@FXML
	MenuItem menuItemExit;
	@FXML
	MenuItem menuItemOpen;
	@FXML
	ImageView imageViewSrc;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		menuItemExit.setOnAction((e) -> {
			textAreaLog.appendText("quit!\n");
			System.exit(-1);
		});

		menuItemOpen.setOnAction((e) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open Resource File");
			ExtensionFilter filter = new ExtensionFilter("写真", Arrays.asList("jpg", "jpeg", "png"));
			fileChooser.setSelectedExtensionFilter(filter);
			File choosedFile = fileChooser.showOpenDialog(new Stage());
			logger.debug(choosedFile.getAbsolutePath());

			Image value = new Image(choosedFile.toURI().toString());
			imageViewSrc.setImage(value);
		});
	}
	// @FXML
	// public void handleButtonAction(ActionEvent event) {
	// // Button was clicked, do something...
	// textAreaLog.appendText("Button Action\n");
	// }

	@FXML
	public void handleButtonAction(ActionEvent event) {
		textAreaLog.appendText("Button Action\n");
	}

	@FXML
	public void andleButtonAction() {
	}

}
